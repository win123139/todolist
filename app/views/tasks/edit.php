<div class="container">

    <!-- add song form -->
    <div>
        <h3>Edit a Work </h3>

        <form  action="<?php echo URL; ?>task/updatework" method="POST">
            <input type="hidden" name="work_id" value="<?php echo htmlspecialchars($work->id, ENT_QUOTES, 'UTF-8'); ?>" />
            <div class="form-group mb-2">
                <label>Work Name : </label>
                <input type="text" class="form-control" name="work_name" value="<?php echo htmlspecialchars($work->work_name, ENT_QUOTES, 'UTF-8'); ?>"  required />
            </div>
            <div class="form-group mb-2">
                <label>Start Date : </label>
                <input type="text" class="form-control" id="update_start_date" name="start_date" value="<?php echo htmlspecialchars($work->start_date, ENT_QUOTES, 'UTF-8'); ?>" required />
            </div>
            <div class="form-group mb-2">
                <label>End Date : </label>
                <input type="text" class="form-control" id="update_end_date" name="end_date" value="<?php echo htmlspecialchars($work->end_date, ENT_QUOTES, 'UTF-8'); ?>" required />
            </div>
            <div class="form-group mb-2">
                <label>Status : </label>
                <select class="form-control" id="status" name="status" va>
                    <?php
                    // Status array
                    $status = Work::getStatusArray();
                    foreach ($status as $statusCode => $statusText) {
                        $selected = $work->status == $statusCode ? 'selected' : '';
                        echo "<option value=\"$statusCode\"  $selected>$statusText</option>";
                    }
                    ?>
                </select>
            </div>
            <button type="submit" style="margin-right: 10px" class="btn btn-primary " name="submit_update_work" >Submit</button>

        </form>
    </div>
</div>

