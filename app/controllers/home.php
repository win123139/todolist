<?php
/**
 * HomeController
 */

class Home extends Controller {
    /*
     * PAGE: index
     */
    function index() {
        //register work model
        $this->model('Work');
        $workModel = new Work();
        // getting all works for calendar view
        $works = $workModel->getWorksForCalendar();
        $viewData = array(
            'works' => $works
        );
        
        // Pass data to view and render
        $this->view('template/header');
        $this->view('home/index',$viewData );
        $this->view('template/footer');
    }

}

?>