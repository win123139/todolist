<?php

/**
 * TaskController
 */
class Task extends Controller {

    /**
     * PAGE: index
     * 
     */
    public function index() {
        // Instance new Model (Work)
        $this->model('Work');
        $workModel = new Work();
        // get all works and amount of works
        $works = $workModel->getAllWorks();
        // get numbere of work not complete
        $workNotComplete = $workModel->getAmountOfWorksNotComplete();
        $viewData = [
            'works' => $works,
            'workNotComplete' => $workNotComplete
        ];
        // load views. within the views we can echo out $works and $amount_of_works easily
        $this->view('template/header');
        $this->view('tasks/index', $viewData);
        $this->view('template/footer');
    }

    /**
     * ACTION: addWork
     * This method handles what happens when you move to  /task/addwork
     */
    public function addWork() {
        // if we have POST data to create a new work entry
        if (isset($_POST["submit_add_work"])) {
            // Instance new Model (Work)
            $this->model('Work');
            $workModel = new Work();
            // validate work input
            if ($workModel->validate($_POST["work_name"], $_POST["start_date"], $_POST["end_date"], $_POST["status"]) == Work::VALID) {
                // do addWork() in model work
                $workModel->addWork($_POST["work_name"], $_POST["start_date"], $_POST["end_date"], $_POST["status"]);
            }
        }

        // redirect to index
        header('location: ' . URL . 'task/index');
    }

    /**
     * ACTION: deleteWork
     * @param int $workId Id of the to-delete work
     */
    public function deleteWork($workId) {
        // if we have an id of a work that should be deleted
        if (isset($workId)) {
            // Instance new Model (Work)
            $this->model('Work');
            $workModel = new Work();
            // do deleteWork() in model/model.php
            $workModel->deleteWork($workId);
        }

        // redirect to index
        header('location: ' . URL . 'task/index');
    }

    /**
     * Page: editWork
     * 
     * @param int $workId Id of the to-edit work
     */
    public function editWork($workId) {
        // if we have an id of a work that should be edited
        if (isset($workId)) {
            // Instance new Model (Work)
            $this->model('Work');
            $workModel = new Work();
            // do getWork() in model
            $work = $workModel->getWork($workId);

            // If the work wasn't found, then it would have returned false, and  display the error page
            if ($work === false) {
                echo 'Error Page';
            } else {
                $viewData = [
                    'work' => $work,
                ];
                // load views. 
                $this->view('template/header');
                $this->view('tasks/edit', $viewData);
                $this->view('template/footer');
            }
        } else {
            // redirect to index
            header('location: ' . URL . 'task/index');
        }
    }

    /**
     * ACTION: updateWork
     *
     */
    public function updateWork() {
        // create a new work entry
        if (isset($_POST["submit_update_work"])) {
            // Instance new Model (Work)
            $this->model('Work');
            $workModel = new Work();
            //validate work input
            if ($workModel->validate($_POST["work_name"], $_POST["start_date"], $_POST["end_date"], $_POST["status"]) == Work::VALID) {
                // do updateWork() from model
                $workModel->updateWork($_POST["work_id"], $_POST["work_name"], $_POST["start_date"], $_POST['end_date'], $_POST['status']);
            }
        }

        // redirect to index
        header('location: ' . URL . 'task/index');
    }

}

?>