<?php

declare(strict_types = 1);
define('ROOTT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
require ROOTT . '/core/testcase.php';
require ROOTT . '/app/models/Work.php';

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class WorkTest extends TestCase {

    use TestCaseTrait;

    private $conn = null;
    private $db = null;

    /**
     * getConnection
     * @return type
     */
    public function getConnection() {
        if ($this->conn === null) {
            try {
                $pdo = new PDO('mysql:host=localhost;dbname=est_calendar', 'root', '');
                $this->conn = $this->createDefaultDBConnection($pdo, 'test');
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }
        return $this->conn;
    }

    /**
     * set up database
     */
    public function setUp() {

        $this->db = $this->getConnection()->getConnection();

        // trumcate table
        $query = $this->db->query('TRUNCATE `tbl_work`');
        // add mockup data
        $query = $this->db->query("INSERT INTO `tbl_work` (`id`, `work_name`, `start_date`, `end_date`, `status`)"
                . " VALUES ('1', 'TestTask', '2018-11-01 00:00:00', '2018-11-02 00:00:00', '1');");
    }

    public function tearDown() {
        
    }

    /**
     * getDataSet
     * @return \PHPUnit_Extensions_Database_DataSet_DefaultDataSet
     */
    public function getDataSet() {

        return new PHPUnit_Extensions_Database_DataSet_DefaultDataSet();
    }

    /**
     * normal case
     */
    public function testValidation01() {

        $work = new Work();
        $result = $work->validate('Meeting', '2018-08-08 07:20:11', '2018-08-08 08:20:11', 1);

        $this->assertTrue($result);
    }

    /**
     * work_name require
     */
    public function testValidation02() {
        $work = new Work();
        $result = $work->validate('', '2018-08-08 07:20:11', '2018-08-08 07:20:11', 1);
        $this->assertFalse($result);
    }

    /**
     * work_name max 255 character
     */
    public function testValidation03() {
        $work = new Work();
        $result = $work->validate('gLdc8TrNuYwPmDl2dRXlCoTDMaEuEzbqWFIOyD8dRIy9Vn5PUdG'
                . 'OlrFdUARRr8zdSBB1BAhrevIJU01q7cCf0bxMGLQtq8n37lSn4TuMLNt55KRwkOKaZToRJEApYejrHdJPqVlyzwzjsiO60U09rvT'
                . 'TLaTb8GW0gAjbNblQcfuKDGZYP1Uph0iDav8b6Kn3F9v6Yr4aj2yv9xsO96v5R0WDbD4'
                . 'm76R2cxBrre9rRm2oT083WiMGduvPNt35c5t', '2018-08-08 07:20:11', '2018-08-08 07:20:11', 1);

        $this->assertTrue($result);
    }

    /**
     * work_name max 256 character
     */
    public function testValidation04() {
        $work = new Work();
        $result = $work->validate('gLdc8TrNuYwPmDl2dRXlCoTDMaEuEzbqWFIOyD8dRIy9Vn5PUdG'
                . 'OlrFdUARRr8zdSBB1BAhrevIJU01q7cCf0bxMGLQtq8n37lSn4TuMLNt55KRwkOKaZToRJEApYejrHdJPqVlyzwzjsiO60U09rvT'
                . 'TLaTb8GW0gAjbNblQcfuKDGZYP1Uph0iDav8b6Kn3F9v6Yr4aj2yv9xsO96v5R0WDbD4'
                . 'm76R2cxBrre9rRm2oT083WiMGduvPNt35c5tM', '2018-08-08 07:20:11', '2018-08-08 07:20:11', 1);

        $this->assertFalse($result);
    }

    /**
     * start_date require
     */
    public function testValidation05() {
        $work = new Work();
        $result = $work->validate('Metting', '', '2018-08-08 07:20:11', 1);

        $this->assertFalse($result);
    }

    /**
     * start_date not in format
     */
    public function testValidation06() {
        $work = new Work();
        $result = $work->validate('Metting', '1234 a', '2018-08-08 07:20:11', 1);

        $this->assertFalse($result);
    }

    /**
     * end_date require
     */
    public function testValidation07() {
        $work = new Work();
        $result = $work->validate('Metting', '2018-08-08 05:20:11', '', 1);

        $this->assertFalse($result);
    }

    /**
     * end_date not in format
     */
    public function testValidation08() {
        $work = new Work();
        $result = $work->validate('Metting', '2018-08-08 05:20:11', '123 07:20:11', 1);

        $this->assertFalse($result);
    }

    /**
     * end_date before start_date
     */
    public function testValidation09() {
        $work = new Work();
        $result = $work->validate('Metting', '2018-08-08 05:20:11', '2018-08-08 01:20:11', 1);

        $this->assertFalse($result);
    }

    /**
     * status not in 1,2,3
     */
    public function testValidation10() {
        $work = new Work();
        $result = $work->validate('Metting', '2018-08-08 05:20:11', '2018-08-08 07:20:11', 4);

        $this->assertFalse($result);
    }

    /**
     * get all row in table
     */
    public function testGetAllWork01() {
        $work = new Work();
        $results = count($work->getAllWorks());

        // read data from database to check 
        $query = $this->db->query('SELECT * FROM tbl_work');
        $number_row = count($query->fetchAll(PDO::FETCH_COLUMN));

        $this->assertEquals($results, $number_row);
    }

    /**
     * add work to table
     */
    public function testAddWork01() {
        $work = new Work();
        $work->addWork('Meeting', '2018-08-08 07:20:11', '2018-08-08 08:20:11', 1);

        // read data from database to check
        $query = $this->db->query('SELECT * FROM tbl_work where work_name = "Meeting" '
                . 'and start_date = "2018-08-08 07:20:11" '
                . 'and end_date = "2018-08-08 08:20:11" '
                . 'and status =1 ');
        $number_row = count($query->fetchAll(PDO::FETCH_COLUMN));
        $this->assertEquals(1, $number_row);
    }

    /**
     * delele work in table
     */
    public function testDeleteWork01() {
        //delete task 1
        $work = new Work();
        $work->deleteWork(1);

        //check database
        $query = $this->db->query('SELECT * FROM tbl_work');
        $number_row = count($query->fetchAll(PDO::FETCH_COLUMN));

        $this->assertEquals(0, $number_row);
    }

    /**
     * get work in table
     */
    public function testGetWork01() {
        $work = new Work();
        $result = $work->getWork(1);

        $this->assertEquals($result->id, 1);
        $this->assertEquals($result->work_name, 'TestTask');
        $this->assertEquals($result->start_date, '2018-11-01 00:00:00');
        $this->assertEquals($result->end_date, '2018-11-02 00:00:00');
        $this->assertEquals($result->status, 1);
    }

    /**
     * update work to table
     */
    public function testUpdateWork01() {

        $work = new Work();
        $work->updateWork(1, 'Meeting 2', '2018-11-08 08:20:11', '2018-11-08 09:20:11', 2);

        // read data from database to check
        $query = $this->db->query('SELECT * FROM tbl_work where work_name = "Meeting 2" '
                . 'and start_date = "2018-11-08 08:20:11" '
                . 'and end_date = "2018-11-08 09:20:11" '
                . 'and status = 2 ');
        $number_row = count($query->fetchAll(PDO::FETCH_COLUMN));

        $this->assertEquals(1, $number_row);
    }

    /**
     * get number of work not complete 
     */
    public function testAmountOfWorksNotComplete01() {
        $work = new Work();
        $result = $work->getAmountOfWorksNotComplete();

        // read data from database to check
        $query = $this->db->query('SELECT * FROM tbl_work where  status <> 3');
        $number_row = count($query->fetchAll(PDO::FETCH_COLUMN));

        $this->assertEquals($result, $number_row);
    }

}
