$(function () {

    $('#start_date').datetimepicker({
        format: 'Y-m-d H:i:s',
        formatDate: 'Y-m-d H:i:s',
        step: 30
      
    });
    $('#end_date').datetimepicker({
        format: 'Y-m-d H:i:s',
        formatDate: 'Y-m-d H:i:s',
        step: 30
    });


    $('#update_start_date').datetimepicker({
        format: 'Y-m-d H:i:s',
        formatDate: 'Y-m-d H:i:s',
        step: 30
    });
    $('#update_end_date').datetimepicker({
        format: 'Y-m-d H:i:s',
        formatDate: 'Y-m-d H:i:s',
        step: 30
    });


});
