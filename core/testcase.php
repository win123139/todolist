<?php

/*
 *  app.php
 * code file end point
 * 
 */

class App {

    private $config = [];
    public $db;

    /**
     * construct function
     */
    function __construct() {

        define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    }

    /**
     * autoload function
     */
    function autoload() {

        spl_autoload_register(function ($class) {

            $class = strtolower($class);
            if (file_exists(ROOT . '/core/classes/' . $class . '.php')) {

                require_once ROOT . '/core/classes/' . $class . '.php';
            } else if (file_exists(ROOT . '/core/helpers/' . $class . '.php')) {

                require_once ROOT . '/core/helpers/' . $class . '.php';
            }
        });
    }

    /**
     * Set config database conetion
     */
    function config() {

        require ROOT . 'core/config/database.php';

        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        try {

            $this->db = new PDO('mysql:host=' . $this->config['database']['hostname'] . ';dbname=' . $this->config['database']['dbname'], $this->config['database']['username'], $this->config['database']['password'], $options);


            // TODO: Remove for production
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {

            echo 'Error: ' . $e->getMessage();
        }
    }

    /**
     * Register path
     * @param type $path
     */
    function regis($path) {

        require ROOT . $path;
    }

    /**
     * start end point controler function
     */
    function start() {
        
    }

}

$app = new App();

$app->autoload();

$app->config();

//$app->start();
//
abstract class Model {

    protected $db;

    /**
     * Setup db
     * @global type $app
     */
    function __construct() {

        global $app;

        $this->db = $app->db;
    }

}

?>