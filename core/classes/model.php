<?php

abstract class Model {

    protected $db;

    /**
     * Setup db
     * @global type $app
     */
    function __construct() {
        global $app;

        $this->db = $app->db;
    }

}

?>