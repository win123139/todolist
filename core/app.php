<?php

/*
 *  app.php
 * code file end point
 * 
 */

class App {

    private $config = [];
    public $db;

    /**
     * construct function
     */
    function __construct() {

        define("URI", $_SERVER['REQUEST_URI']);
        define("ROOT", $_SERVER['DOCUMENT_ROOT']);
        define('URL_PUBLIC_FOLDER', 'public');
        define('URL_PROTOCOL', '//');
        define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
        define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
        define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);
    }

    /**
     * autoload function
     */
    function autoload() {

        spl_autoload_register(function ($class) {

            $class = strtolower($class);
            if (file_exists(ROOT . '/core/classes/' . $class . '.php')) {

                require_once ROOT . '/core/classes/' . $class . '.php';
            } else if (file_exists(ROOT . '/core/helpers/' . $class . '.php')) {

                require_once ROOT . '/core/helpers/' . $class . '.php';
            }
        });
    }

    /**
     * Set config database conetion
     */
    function config() {

        $this->regis('/core/config/session.php');
        $this->regis('/core/config/database.php');

        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        try {

            $this->db = new PDO('mysql:host=' . $this->config['database']['hostname'] . ';dbname=' . $this->config['database']['dbname'], $this->config['database']['username'], $this->config['database']['password'], $options);


            // TODO: Remove for production
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {

            echo 'Error: ' . $e->getMessage();
        }
    }

    /**
     * Register path
     * @param type $path
     */
    function regis($path) {

        require ROOT . $path;
    }

    /**
     * start end point controler function
     */
    function start() {

        session_name($this->config['sessionName']);
        session_start();

        $route = explode('/', URI);

        $route[1] = strtolower($route[1]);

        if (file_exists(ROOT . '/app/controllers/' . $route[1] . '.php')) {
            $this->regis('/app/controllers/' . $route[1] . '.php');
            $controller = new $route[1]();
        } else {
            $this->regis('/app/controllers/home.php');
            $main = new Home();
        }
    }

}

?>