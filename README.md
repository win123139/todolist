
# ToDoList

## Features

- Functions to Add, Edit, Delete Work. A work includes information of “Work Name”, “Starting Date”, “Ending Date” and “Status” (Planning, Doing, Complete)

- Function to show the works on a calendar view with: Date, Week, Month ( use third-party library)

## Requirements

- PHP 5.6 or PHP 7.0
- MySQL
- Composer


## Manual Installation

1. Edit the database credentials in `core/config/database.php`
2. Execute the est_calendar.sql statements in the `_install/`-folder (with PHPMyAdmin for example).
3. Install composer and run `composer install` in the project's folder to create the PSR-4 autoloading and codeception for unit test
4. run commannd 'php -S localhost:8000' in root folder
## Manual Installation unit test

1. Edit the database credentials in `testcase/WorkTest` function getConnection with your database test
2. Execute the  unit test by  `./vendor/bin/phpunit --bootstrap vendor/autoload.php testcase/WorkTest` in root folder of project

if have problem please delete vendor folder and run composer install againt

